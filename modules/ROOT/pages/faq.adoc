= Frequently Asked Questions (FAQ)

== About the project

Where the {variant-name} comes from?::
    {variant-name} comes from Terminalia Sericea and has been chosen due to the many connections with this project:
    * Sericea starts with “S”, like Sway
    * Terminalia Sericea is a tree. So is ostree.
    * Terminalia Sericea is also known as Silverleaf. Silverleaf was one of the top contenders for the project name of what later became Fedora Silverblue
    * If you stare at a Terminalia Sericea for a while, you’ll eventually start noticing similarities with the Sway logo

What is {variant-name}'s relationship with Project Atomic?::
    {variant-name} uses the same core technology as Fedora Atomic Host (as well as its successor, Fedora CoreOS).
    However, {variant-name} is specifically focused on workstation/desktop use cases.

== About using {variant-name}

Can I use {variant-name} with an Nvidia GPU?::

The proprietary Nvidia driver is not officially supported.
It _may_ work, but we are unable to provide any help with the configuration or resolve any issues.
Check the Nvidia section of the xref:troubleshooting.adoc#_using_nvidia_drivers[troubleshooting guide] if you absolutely have to use it.
+
The open-source driver (Nouveau) should work fine in general, but currently https://nouveau.freedesktop.org/FeatureMatrix.html[lacks support] for the most recent generations of the Nvidia hardware. +
The initial support for *Ampere* (30xx) GPU series is expected to appear in kernel 6.2/mesa 23.0 and hopefully will be ready by Fedora 38 release.
There are no estimations for *Ada Lovelace* (40xx) GPU series support.
+
NOTE: Lack of support in Nouveau also means that laptops with discrete Ampere (or later) GPUs https://gitlab.freedesktop.org/wlroots/wlroots/-/issues/3537[may fail to use external monitors], as the corresponding ports are routed through the Nvidia GPU.

How can I install Eclipse on {variant-name}?::
    Instructions to setup the Nightly flatpak remote for Eclipse are available http://eclipse.matbooth.co.uk/flatpak[here].

How do I create a VPN connection?::
    `/etc` is not part of the immutable OS image, so you can just copy files into `/etc/NetworkManager/system-connections` (or let NetworkManager store them there when you recreate your connections).
    Certificates in `/etc/pki` need to be handled similarly.

How can I play more videos in Firefox, like YouTube?::
    Firefox is included in the OS image for now.
    Until that changes, getting it to play videos works the same way as it does for the regular Fedora Workstation: find a package with the needed codecs, and install it.
    The one difference is that you use `rpm-ostree install` instead of `dnf install`.
    An alternative solution is to install the nightly Firefox, which is available as a https://firefox-flatpak.mojefedora.cz/org.mozilla.FirefoxNightly.flatpakref[Flatpak].

How can I see what packages were updated between two commits?::

* If you want to compare the booted deployment with the pending deployment (or rollback deployment), simply issue:

 $ rpm-ostree db diff
+
TIP: You can also see the RPM changelog by adding the `-c` option like so: `rpm-ostree db diff -c`

* If you want to see which packages were updated between two specific commits:

. find out which two commits you want to compare by issuing:

 $ ostree log <ref>

. you can now compare the two commits by issuing:

 $ rpm-ostree db diff <commit x> <commit y>

How can I check the version number of an installed package?::

You can simply use:

 $ rpm -q <package>

How can I check if an `rpm` software package is available in the repository?::

At this point in time, there is no `rpm` package search function built into `rpm-ostree`.
However, you can use `toolbox` with the following command:

 $ toolbox run dnf search <package>
+
NOTE: The assumption is that you have already created a toolbox matching the version of your {variant-name} installation.

How can I downgrade my system's kernel?::

If, for whatever reason, you need to downgrade the kernel, you can do so by following these steps:

. For the version you need to downgrade, download `<kernel>`, `<kernel-core>`, `<kernel-modules>` and `<kernel-modules-extra>` from https://koji.fedoraproject.org/koji/packageinfo?packageID=8[Koji].

. Install the packages downloaded on the previous step by issuing:

 $ rpm-ostree override replace <kernel> <kernel-core> <kernel-modules> <kernel-modules-extra>

. Reboot the system to apply the changes.

[[pinning]]How can I upgrade my system to the next major version (for instance: rawhide or an upcoming Fedora release branch), while keeping my current deployment?::

OSTree allows you to pin deployments (pinning ensures that your deployment of choice is kept and not discarded).

. Assuming that you want to keep your default deployment, issue the following command:

 $ sudo ostree admin pin 0
+
NOTE: `0` here refers to the first deployment listed by `rpm-ostree status`

. Verify that you have pinned your deployment of choice by issuing:

 rpm-ostree status

. After the deployment is pinned, you can upgrade your system by using the instructions found xref:updates-upgrades-rollbacks.adoc#upgrading[here].

. When you have completed rebasing, reboot the system.
The GRUB menu will now present you with both: the previous deployment major version entry (e.g.: *"Fedora 30.YYYYMMDD.n"*) and the new deployment major version entry (e.g.: *"Fedora 31.YYYYMMDD.n"*).
+
NOTE: At the moment it is not possible to name (pinned) deployments and their associated GRUB menu entries.
